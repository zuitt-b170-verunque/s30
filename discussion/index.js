let express = require ("express");
const mongoose = require("mongoose");

const app = express();
const port = 3000;


mongoose.connect("mongodb+srv://verunque_david:password1210@wdc028-course-booking.13hym.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology : true
})

// notif for connectino success or failure
let db= mongoose.connection;
// if an error existed in connecting to MongoDB
db.on("error", console.error.bind(console, "Connection Error"))
// if the connection is successful
db.once("open", () => console.log("We're connected to the database"))


app.use(express.json()); //so it can read JSON
app.use(express.urlencoded({extended:true})); //so it can read files from the forms


// Mongoose Schema - sets the structure of the document that is to be created; serves as the blueprint to the data/record
const taskSchema = new mongoose.Schema({
    name: String,
    status:{
        type:String,
        default:"pending"
    }
})




const Task = mongoose.model("Task", taskSchema)


// ROUTES
/* 
business logic:
1. check if the task is already existing
if the task exist, return "there is a duplicate"
if not existing, add the task in the database

2. the task will come from request body (req.body)

3. create a new task object with the needed properties

4. save to database

*/

app.post("/tasks", (req, res)=>{
    Task.findOne({name:req.body.name}, (error, result)=> {
        if(result!==null && result.name === req.body.name)
        {
            return res.send("There is a duplicate");
        }
        else
        {
            let newTask = new Task ({
                name: req.body.name
            })
            // saveErr - parameter that accepts errors
            // savedTask - parameter that accepts the object if successful
            newTask.save((saveErr, savedTask)=>{
                if(saveErr){
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("New Task Created")
                }
            })
        }
    })
})



app.get("/tasks", (req, res) => {
    Task.find({}, (error, result)=> {
        if (error){
            return console.log(err)
        }else{
            return res.status(200).json({data:result}) 
        }
    })
})






// create a function/route for a user registration
/*
1. Find if there is a duplicate user using the username field
    - if the user is existing, return "username already in use"
    - if the user is not existing, add it in the database
        - if the username and the password are filled, save
            - in saving, create a new User object (a User schema)
                - if there is an errror, log it in the console
                - if there is no error, send a status of 201 and "successfully registered"
        - if one of them is blank, send the response "BOTH username and password must be provided"
*/

const Registers = new mongoose.Schema({
    name : String,
    password : String
})

const User = mongoose.model("User", Registers)


app.post("/register", (req, res) =>{
    console.log(`can connect`)
    User.findOne({username:req.body.username}, (error, result) => {
    // User.findOne({username:req.body.username}, (error, result) => {
        console.log(`can check if user exist`)

        if (result !==null && result.username === req.body.username) {
            return res.send ("Username already exists");


        } /* else if (result.username == null || result.password == null){
            res.send(`BOTH username and password must be provided`);

        } */
         else {
            console.log(`checking else`)
            let newUser = new User([{
                username : req.body.username,
                // username : req.body.username,
                password : req.body.Password
            }])
            newUser.save((saveErr, success)=>{
                if(saveErr){
                    return console.log(saveErr)
                } else {
                    return res.status(201).send("Succesfully Registered")
                }
            })

        }
    })
})

// create a get request for getting all registered users

/*
    - retrieve all users registerd using find functionality
    - if there is an error, log it in the console
    - if there is no error, send a success status back to the client and return the array of users
*/

app.get("/tasks", (req, res) => {
    User.find({}, (error, result)=> {
        if (error){
            return console.log(err)
        }else{
            return res.status(200).json({data:result}) 
        }
    })
})




app.listen(port, ()=> console.log(`Server is running at port ${port}`)); //listen to port, and then the message if successful